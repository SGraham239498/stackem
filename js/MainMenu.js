
Game.MainMenu = function (game) {

    this.bgImage = null;
    this.music = null;
    this.playButton = null;

};

Game.MainMenu.prototype = {

    create: function () {

        //	We've already preloaded our assets, so let's kick right into the Main Menu itself.
        //	Here all we're doing is playing some music and adding a picture and button
        //	Naturally I expect you to do something significantly better :)

        //this.music = this.add.audio('titleMusic');
        //this.music.play();

        this.bgImage = this.add.sprite(0, 0, 'MenuBG');
        //this.bgImage.scale.setTo(5,8);

        this.playButton = this.add.button(this.game.world.width/2 + 100, this.game.world.height/2, 'playButton', this.startGame, this, 0,1,0);

    },

    update: function () {

        //	Do some nice funky main menu effect here
        if (this.input.keyboard.isDown(Phaser.Keyboard.B))
        {
            this.state.start('Game');
        }
        //if (Phaser.Rectangle.contains(this.playButton.body, this.input.x, this.input.y))
        //if(this.input.mousePointer.isDown)
        //  this.state.start('Game');
    },

    startGame: function (pointer) {

        //	Ok, the Play Button has been clicked or touched, so let's stop the music (otherwise it'll carry on playing)
        //this.music.stop();

        //	And start the actual game
        this.state.start('Game');

    }

};
