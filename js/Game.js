/**
 * Uses the base framework from project templates
 * @author Shane Graham and Ian Young
 * @param game
 * @constructor
 */
Game.Game = function (game) {

    //	When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    this.game;		//	a reference to the currently running game
    this.add;		//	used to add sprites, text, groups, etc
    this.camera;	//	a reference to the game camera
    this.cache;		//	the game cache
    this.input;		//	the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
    this.load;		//	for preloading assets
    this.math;		//	lots of useful common math operations
    this.sound;		//	the sound manager - add a sound, play one, set-up markers, etc
    this.stage;		//	the game stage
    this.time;		//	the clock
    this.tweens;    //  the tween manager
    this.state;	    //	the state manager
    this.world;		//	the game world
    this.particles;	//	the particle manager
    this.physics;	//	the physics manager
    this.rnd;		//	the repeatable random number generator

    //	You can use any of these from any function within this State.
    //	But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.
    // this.cursors = null;
    this.ground;
    this.groundCollisionGroup;
    this.cart;
    this.moveBehaviour;
    this.cartCollisionGroup;
    this.cans;
    this.CanCollisionGroup;
    this.score = 0;
    this.scoreText;
    this.timer;
    this.stackCounter = 0;
    this.stack;
    this.gameTime = 60;
    this.spriteMaterial;
    this.contactMaterial;
    this.cartThrust =0.1;
};

var cartThrust = 0;

Game.Game.prototype = {

    create: function () {

        //add the background
        this.add.sprite(0,0, 'background');
        //add timer
        this.timer = new Timer();
        this.timer.background = this.add.sprite(330,0,'timer');
        this.timer.timeValue = this.add.text(380,10, ''+this.gameTime, { font: '28px Arial', fill: '#fff' });
        //add stack guauge
        //add player
        //add score counter
        scoreString = 'Score : ';
        this.scoreText = this.add.text(10, 10, scoreString + this.score, { font: '32px Arial', fill: '#fff' });
        //start the p2 physics system
        this.physics.startSystem(Phaser.Physics.P2JS);
        this.physics.p2.gravity.y = 150;
        this.physics.p2.restitution = 0.0;
        this.physics.p2.friction = 500.0;
        this.physics.p2.applyDamping = true;
        this.physics.p2.enableBodySleeping = true;
        this.physics.p2.setImpactEvents(true);
        //setup collision groups
        this.groundCollisionGroup = this.physics.p2.createCollisionGroup();
        this.CanCollisionGroup = this.physics.p2.createCollisionGroup();
        this.cartCollisionGroup = this.physics.p2.createCollisionGroup();
        this.physics.p2.updateBoundsCollisionGroup();
        //create ground and cart objects
        this.createGround();
        this.createCart();

        //setup contact materials
        this.spriteMaterial = this.physics.p2.createMaterial('spriteMaterial', this.cart.body);
        this.contactMaterial = this.physics.p2.createContactMaterial(this.spriteMaterial,this.spriteMaterial);

        this.contactMaterial.friction = 1000;     // Friction to use in the contact of these two materials.
        this.contactMaterial.restitution = 0.0;  // Restitution (i.e. how bouncy it is!) to use in the contact of these two materials.
        this.contactMaterial.stiffness = 10;    // Stiffness of the resulting ContactEquation that this ContactMaterial generate.
        this.contactMaterial.relaxation = 30;     // Relaxation of the resulting ContactEquation that this ContactMaterial generate.
        this.contactMaterial.frictionStiffness = 10;    // Stiffness of the resulting FrictionEquation that this ContactMaterial generate.
        this.contactMaterial.frictionRelaxation = 300;     // Relaxation of the resulting FrictionEquation that this ContactMaterial generate.
        this.contactMaterial.surfaceVelocity = 0.0;        // Will add surface velocity to this material. If bodyA rests on top if bodyB, and the surface velocity is positive, bodyA will slide to the right.
        this.contactMaterial.frictionCoefficient = 1000.0;
        //setup timed events
        this.time.events.loop(Phaser.Timer.SECOND * 3,this.createCan,this);
        this.time.events.loop(Phaser.Timer.SECOND * 1,this.decreaseTimer,this);
        this.time.events.loop(Phaser.Timer.SECOND * 1,this.updateText,this);
        this.time.events.loop(Phaser.Timer.SECOND * 1,this.updateScore,this);
        this.time.events.add(Phaser,Timer.SECOND * 60,this.pauseGame,this);

        //display controls guide

         /**
         * Defines the movement behaviour of the cart object, on a mouse down event
         * increase the thrust to be applied to its velocity
         *
         * @method cartMovement
         * @param pointer
         */
        var cartmovement = function(pointer){

            if (this.game.input.activePointer.isDown)
            {
                 if(this.game.input.x < this.body.x-6)
                //  400 is the speed it will move towards the mouse
                    //this.cart.body.moveLeft(400);
                    cartThrust = -400;
                else if(this.game.input.x > this.body.x+6)
                    //this.cart.body.moveRight(400);
                    cartThrust = 400;
                else cartThrust = 0;
            }


        }
        this.cart.movement = cartmovement;

        stopThrust = function(){
            cartThrust = 0;
        }

        //this.input.onDown.add(mousedown,this);
        this.input.onUp.add(stopThrust,this);

        this.cart.update = function(){
            this.body.velocity.x += cartThrust;
            this.body.velocity.y = 0;
        }

        //setup the stack
        this.stack = new Array();

    },

    update: function () {

        this.cart.movement();
        this.cart.update();
        //if (this.input.keyboard.isDown(Phaser.Keyboard.ESC))
        //this.quitGame();
        if(this.stack.length >= 5)
        this.consumeStack();
        if(this.gameTime === 0)
            var score = this.add.text(200, 250, 'You Scored: ' + this.score, { font: '48px Arial', fill: 'red' });
    },

    updateText : function(){
        this.scoreText.setText('Score : ' + this.score,this);

    },

    updateScore : function(){
       // this.score+=this.stackCounter*10;
    },

    /**
     * Create the cart object used to catch the falling cans. sets its sprite with the appropriate image,
     * defines its properties for use with the physics system and sets up collision behaviour
     *
     * @method createCart
     */
    createCart: function(){
        this.cart = this.add.sprite(400,480,'cart');
        this.physics.p2.enable(this.cart, false);
        this.cart.body.motionState = Phaser.Physics.P2.Body.KINEMATIC;
        this.cart.body.collideWorldBounds = true;
        this.cart.body.setCollisionGroup(this.cartCollisionGroup);
        this.cart.body.collides([this.CanCollisionGroup]);
        this.cart.body.data.gravityScale = 0;
        this.cart.body.mass = 9999;
        this.cart.body.data.fixedRotation = true;
        this.cart.body.onBeginContact.add(this.cartCollide,this);
        this.cart.body.onEndContact.add(this.canLeave,this);
        this.cart.body.setMaterial(this.spriteMaterial);

    },

    /**
     * collision response between cart and bad can, destroys the current stack
     *
     * @method cartCollide
     * @param body
     * @param shape1
     * @param shape2
     * @param equation
     */
    cartCollide : function(body, shape1, shape2, equation){
        var tolerance = 1;
        if(body.sprite.key == 'badCan'){
            body.clearShapes();
            body.sprite.kill();
            this.stackCounter = 0;
            this.blowStack();
        }

    },

    /**
     * Create the can object and define its type by using a random number within a range to be used in a switch statement.
     * sets its sprite with the appropriate image from the selection and
     * defines its properties for use with the physics system and sets up collision behaviour
     *
     * @method createCan
     */
    createCan: function() {
        if(this.gameTime > 0){
            var selection = this.rnd.integerInRange(1,4);
            var pos = this.rnd.integerInRange(100,700);
            var can;
            if(selection == 1){
                can = this.add.sprite(pos,0,'badCan',4);
                this.physics.p2.enable(can, false);
                can.body.setCollisionGroup(this.CanCollisionGroup);
                can.body.collides([this.CanCollisionGroup]);
                can.body.collides(this.groundCollisionGroup);
                can.body.collides([this.cartCollisionGroup]);
                can.body.mass = 10;
                can.body.onBeginContact.add(this.badCanCollide,this);

            }
            else{
                if(selection == 2){
                    can = this.add.sprite(pos,0,'redCan');
                    can.points = 10;
                }
                else if(selection == 3){
                    can = this.add.sprite(pos,0,'blueCan');
                    can.points = 5;
                }
                else if(selection == 4){
                    can = this.add.sprite(pos,0,'orangeCan');
                    can.points = 3;
                }

                this.physics.p2.enable(can,false);
                //can.body.mass = 10;
                // may need to set rectangle
                can.body.setRectangle(45,52);
                can.body.setMaterial(this.spriteMaterial);
                can.body.setCollisionGroup(this.CanCollisionGroup);
                can.body.collides([this.CanCollisionGroup]);
                can.body.collides(this.groundCollisionGroup);
                can.body.collides(this.cartCollisionGroup);
                can.body.createGroupCallback(this.CanCollisionGroup,this.hitcans,this);
                can.body.createGroupCallback(this.cartCollisionGroup,this.hitcans,this);
                //can.body.onBeginContact.add(this.canCollide,this);
               // can.body.onEndContact.add(this.canLeave,this);
                can.body.collided = false;
                can.body.onstack = false;
            }
        }

       //can.body.onEndContact.add(this.canLeave,this);

    },

    /**
     * function callback for can collision, if the can is within the boundary of
     * the body on collision it will be added to the stack
     *
     * @method hitcans
     *
     * @param body
     * @param body2
     */
    hitcans: function(body,body2){
        // if within boundary
        var boundary = 10;
        if(body2.sprite.key === 'cart')
        boundary = this.cart.width/2;

        if (body.x > body2.x-boundary && body.x < body2.x+boundary)
        {

            if (this.stack.length < 6)
            {
                if (!body.onstack)
                {
                    this.score += 1;
                    body.onstack = true;
                    this.stack.push(body.sprite);
                    body.setZeroVelocity();
                    body.data.gravityScale = 1;
                    body.data.fixedRotation=true;
                    body.mass = 5;
                    //body.createGroupCallback(this.CanCollisionGroup,null,this);
                    body.onEndContact.addOnce(this.canLeave,this);
                }
                //body.onBeginContact.remove(this.canCollide,this);
                //body.onBeginContact.add(this.cartCollide, this);
            }

        }
    },

    badCanCollide : function(body, shape1, shape2, equation){
        if(body.sprite.key!='ground' && body.sprite.key!='cart'){
            body.clearShapes();
            body.sprite.kill();
            this.stackCounter --;
            if(body.onstack)
            this.stack.pop();
        }
    },

    badCanEndCollide : function(body, shape1, shape2, equation){

    },

    canLeave:function(body, shape1, shape2, equation){
        if(body.onstack && body.x < this.cart.x-this.cart.width && body.x > this.cart.x+ this.cart.width){

            this.stack.pop();
        }
    },

    /**
     * This method is the contact behaviour of the ground,
     * on collision with a can it will clear the cans physics data and remove it from the world
     *
     * @method removeCan
     *
     * @param body
     * @param shape1
     * @param shape2
     * @param equation
     */
    removeCan: function (body, shape1, shape2, equation){
       //if the body is a can game object, remove it from the game
        body.clearShapes();
        body.sprite.kill();
    },

    /**
     * This is called when the stack limit has been reached, it iterates through the stack removing the objects
     * and applying each objects points value to the score
     *
     * @method consumeStack
     */
    consumeStack: function(){
        for (var i in this.stack){
            this.stack[i].body.clearShapes();
            this.stack[i].kill();
            this.score += this.stack[i].points;
            this.stack.pop();
        }
    },

    /**
     * This is called when in the callback for the collision between a bad can and the cart
     * it iterates through the stack removing the objects
     *
     * @method blowStack
     */
    blowStack: function(){
        for (var i in this.stack){
        this.stack[i].body.clearShapes();
        this.stack[i].kill();
        this.stack.pop();
        }
    },

    /**
     * Create the ground object. sets its sprite with the appropriate image,
     * defines its properties for use with the physics system and sets up collision behaviour
     *
     * @method createGround
     */
    createGround: function (){

        this.ground = this.add.sprite(400, this.world.height - 32, 'ground');
        this.physics.p2.enable(this.ground,false);
        this.ground.body.motionState = 2;
        this.ground.body.data.gravityScale = 0;
        this.ground.body.mass = 9999;
        this.ground.body.data.fixedRotation=true;
        this.ground.body.setCollisionGroup(this.groundCollisionGroup);
        this.ground.body.collides(this.CanCollisionGroup);//,this.removeCan,this);
        this.ground.body.collides(this.cartCollisionGroup);//,this.removeCan,this)
        this.ground.body.onBeginContact.add(this.removeCan,this);
    },

    /**
     * Decreases the timer value by one so long as it is above zero,
     * if there is only 10 seconds remaining change the colour to red to highlight to player
     *
     * @method decreaseTimer
     */
    decreaseTimer: function (){
        if(this.gameTime > 0){
            this.gameTime -= 1;
            if(this.gameTime <= 10)
                this.timer.timeValue.setStyle({font: '28px Arial', fill: 'red'});
                this.timer.timeValue.setText( ''+this.gameTime);
        }
    },

    //callback functions to pause/unpause game
    unPauseGame: function (){
        this.isRunning = true;
    },

    pauseGame: function (){

        this.isRunning = false;
    },

    //currently not working
    quitGame: function () {

        //	Then let's go back to the main menu.
        this.state.start('MainMenu');

    },

    gameOver: function(){

    },

    render: function (){

       // this.game.debug.text('stack: ' + this.stackCounter, 400, 580);
        //this.game.debug.text('stacksize: ' + this.stack.length, 400, 20);
    }

};
