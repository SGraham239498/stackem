
Game.Preloader = function (game) {

    this.background = null;
    this.preloadBar = null;

    this.ready = false;
    this.cursors = null;


};

Game.Preloader.prototype = {

    preload: function () {

        //	These are the assets we loaded in Boot.js
        //	A nice sparkly background and a loading progress bar
        this.background = this.add.sprite(0, 0, 'preloaderBackground');
        this.preloadBar = this.add.sprite(300, 300, 'preloaderBar');

        //	This sets the preloadBar sprite as a loader sprite.
        //	What that does is automatically crop the sprite from 0 to full-width
        //	as the files below are loaded in.
        this.load.setPreloadSprite(this.preloadBar);
        this.load.spritesheet('playButton', 'img/sbuttonover.png', 150,50);
        //this.input.createCursorKeys();
        //	Here we load the rest of the assets our game needs.
        this.load.image('MenuBG', 'img/MenuScreen.png');
        this.load.image('ground', 'img/ground.png');
        this.load.image('background', 'assets/backgrounds/sky.png');
        this.load.spritesheet('redCan','img/redanims.png',53,60,29);
        this.load.spritesheet('blueCan','img/blueanims.png',53,60,29);
        this.load.spritesheet('orangeCan','img/orangeanims.png',53,60,29);
        this.load.spritesheet('badCan','img/blockercans.png',53,60,12);
        this.load.image('timer', 'img/timerBG.png');//foreground image?
        this.load.image('stackguage', 'img/stackgauge.png');//foreground image?
        this.load.image('cart', 'img/cartBody.png');
        this.load.image('wheel', 'img/wheel.png');

        //this.load.audio('titleMusic', ['audio/main_menu.mp3']);
        //this.load.bitmapFont('caslon', 'fonts/caslon.png', 'fonts/caslon.xml');
        //	+ lots of other required assets here

    },

    create: function () {

        //	Once the load has finished we disable the crop because we're going to sit in the update loop for a short while as the music decodes
        this.preloadBar.cropEnabled = false;
        this.cursors = this.input.keyboard.createCursorKeys();
        //this.cursors = this.game.input.keyboard.createCursorKeys();

    },

    update: function () {

        //	You don't actually need to do this, but I find it gives a much smoother game experience.
        //	Basically it will wait for our audio file to be decoded before proceeding to the MainMenu.
        //	You can jump right into the menu if you want and still play the music, but you'll have a few
        //	seconds of delay while the mp3 decodes - so if you need your music to be in-sync with your menu
        //	it's best to wait for it to decode here first, then carry on.

        //	If you don't have any music in your game then put the game.state.start line into the create function and delete
        //	the update function completely.

        /*if (this.cache.isSoundDecoded('titleMusic') && this.ready == false)
         {
         this.ready = true;
         this.state.start('MainMenu');
         }*/
        /*if (this.input.keyboard.isDown(Phaser.Keyboard.UP))
         {
         this.state.start('MainMenu');
         }
         if (this.cursors.left.isDown)
         {
         this.state.start('MainMenu');
         }*/
        this.state.start('MainMenu');
    }

};
