var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update: update });

var sprite;
var cursors;

function preload() {

    game.load.image('vu', 'img/cartbody.png');
    game.load.image('ball', 'img/button.png');
    game.load.image('sky', 'img/loadingscreen.png');

}

function create() {

    game.add.image(0, 0, 'sky');

    //	Enable p2 physics
    game.physics.startSystem(Phaser.Physics.P2JS);

    //  Add 2 sprites which we'll join with a constraint
    sprite = game.add.sprite(400, 200, 'ball');

    var vu1 = game.add.sprite(400, 250, 'vu');

    game.physics.p2.enable([sprite, vu1]);

    //  Lock the two bodies together. The [0, 50] sets the distance apart (y: 80)
    game.physics.p2.createLockConstraint(sprite, vu1, [0, 60], 0);
    game.physics.p2.addConstraint(constraint);
    text = game.add.text(20, 20, 'move with arrow keys', { fill: '#ffffff' });

    cursors = game.input.keyboard.createCursorKeys();

}

function update() {

    if (game.input.activePointer.isDown)
    {
        sprite.body.moveLeft(100);
    }



}